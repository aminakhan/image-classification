# -*- coding: utf-8 -*-
"""prime numbers code.ipynb

Automatically generated by Colaboratory.

Original file is located at
    https://colab.research.google.com/drive/1K19yix88PjvNVuL5oO0zhtSdZJggPei_
"""

def is_prime(number):
 if number <= 1:
    return False
 if number <= 3:
   return True
 if number % 2 ==0 or number % 3 == 0:
  return False
 i=5
 while i * i <= number:
   if number % i == 0 or number % (i+2) == 0:
      return False
   i+=6
 return True
for num in range (2,101):
    if is_prime(num):
       print(num,"is a prime number")
    else:
       print(num,"is not a prime number")